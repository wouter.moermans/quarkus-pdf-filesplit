package org.wooter;

import io.smallrye.mutiny.Uni;
import lombok.extern.jbosslog.JBossLog;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
@JBossLog
public class GreetingResource {

    private final FileService fileService;

    long startTime;

    public GreetingResource(FileService fileService) {
        this.fileService = fileService;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Uni<Boolean> hello() {
        return Uni.createFrom().voidItem()
                .onItem().invoke(() -> startTime = System.nanoTime())
                .onItem().transformToUni((a) -> fileService.doLogic())
                .onItem().invoke(() -> {
                    long endTime = System.nanoTime();
                    long duration = endTime - startTime;
                    log.infof("Rendered %d page%s in %dms%n", 13, 13 == 1 ? "" : "s",
                            duration / 1000000);
                });
    }
}