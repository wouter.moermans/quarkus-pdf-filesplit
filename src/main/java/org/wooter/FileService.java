package org.wooter;

import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.infrastructure.Infrastructure;
import io.smallrye.mutiny.unchecked.Unchecked;
import lombok.extern.jbosslog.JBossLog;
import org.apache.commons.io.FilenameUtils;
import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;

import javax.enterprise.context.ApplicationScoped;
import java.io.File;
import java.util.stream.IntStream;

@JBossLog
@ApplicationScoped
public class FileService {
    public FileService() {

    }

    File inFile = new File("C:\\Users\\woute\\Downloads\\WWTAN_783AF09F160942FF90137FF3E243842C.pdf");
    String outputPrefix = FilenameUtils.removeExtension(inFile.getAbsolutePath());
    String imageFormat = "jpg";
    int dpi = 300;
    private ImageType imageType = ImageType.RGB;
    //0 for png 1 for jpg
    private float quality = 1;
    private boolean showTime = true;

    public Uni<Boolean> doLogic() {
        return getDocument()
                .onItem()
                .transformToUni(this::renderImage);
    }

    private Uni<PDDocument> getDocument() {
        return Uni.createFrom().item(Unchecked.supplier(() -> Loader.loadPDF(inFile)))
                .onItem().invoke(Unchecked.consumer((doc) -> {
                    PDAcroForm acroForm = doc.getDocumentCatalog().getAcroForm();
                    if (acroForm != null && acroForm.getNeedAppearances()) {
                        acroForm.refreshAppearances();
                    }
                }));
    }

    private Uni<Boolean> renderImage(PDDocument document) {

        var unis = IntStream.rangeClosed(0, document.getNumberOfPages() - 1)
                .mapToObj
                        (
                                page -> Uni.createFrom().item(Unchecked.supplier(() -> {
                                            var doc = Loader.loadPDF(inFile);
                                            PDAcroForm acroForm = doc.getDocumentCatalog().getAcroForm();
                                            if (acroForm != null && acroForm.getNeedAppearances()) {
                                                acroForm.refreshAppearances();
                                            }
                                    return new PDFRenderer(doc).renderImageWithDPI(page, dpi, imageType);
                                        }))
                                        .map(Unchecked.function(image -> {
                                            String fileName = outputPrefix + "-" + (page + 1) + "." + imageFormat;
                                            return ImageIOUtil.writeImage(image, fileName, dpi, quality);
                                        }))
                                        .runSubscriptionOn(Infrastructure.getDefaultExecutor())
                        ).toList();

        return Uni.join().all(unis).usingConcurrencyOf(15).andFailFast().map(lb -> !lb.contains(false));
    }


}
